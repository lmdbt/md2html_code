# md2html_code

## À propos

"md2html_code" est une application web simple conçue pour faciliter la conversion de Markdown en HTML. Elle est particulièrement utile pour rédiger rapidement des courriels, des articles, et d'autres contenus en HTML lorsque Markdown est disponible, comme sur des plateformes telles que Magistère ou SPIP pour les sites web académiques.

## Fonctionnalités

- **Conversion en Temps Réel** : Convertit le Markdown en HTML en temps réel.
- **Affichage du Code HTML** : Affiche le code HTML généré au lieu de son aperçu.
- **Copie dans le Presse-Papiers** : Permet de copier facilement le code HTML généré.
- **Téléchargement du Code** : Offre la possibilité de télécharger le code HTML généré sous forme de fichier `.html`.

## Comment Utiliser

1. **Écrire le Markdown** : Commencez par écrire ou coller votre Markdown dans la zone de texte de gauche.
2. **Voir le Code HTML** : Le code HTML généré s'affiche automatiquement dans la zone de droite.
3. **Copier ou Télécharger** : Utilisez les boutons en haut de la page pour copier le code dans le presse-papiers ou pour télécharger le fichier HTML.

## Cas d'Usage

- Rédaction de **courriels** avec mise en forme avancée.
- Création de **contenus web** pour des plateformes éducatives.
- **Conversion rapide** de notes Markdown en documents HTML.

## Technologies Utilisées

- HTML/CSS pour la structure et le style de l'application.
- JavaScript pour la logique de conversion et d'interaction.
- [markdown-it](https://github.com/markdown-it/markdown-it) pour la conversion de Markdown en HTML.

## Contribution

Toutes les contributions sont les bienvenues. Si vous avez des suggestions d'amélioration ou des corrections, n'hésitez pas à ouvrir une issue ou une pull request.

## Licence

Cette application est mise à disposition sous une licence libre (C0). Vous êtes libre de l'utiliser, de la modifier et de la distribuer selon vos besoins.
